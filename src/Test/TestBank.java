package Test;

import bank.*;

public class TestBank {
    public static void main(String[] args) throws Exception {
        Bank bank = new Bank(args[0], Integer.parseInt(args[1]));
        long acno1 = bank.openAccount(Account.Type.SAVINGS, "name1", 10000);
        bank.listAccounts();
        long acno2 = bank.openAccount(Account.Type.DEPOSIT, "deposit ac holder", 10000);
        bank.deposit(acno1, 5000);
        bank.listAccounts();
        bank.deposit(acno2, 10000);
        bank.listAccounts();
    }
}
