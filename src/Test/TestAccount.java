package test;

import bank.*;
import java.util.Arrays;
import java.util.Comparator;
import bank.util.NameComparator;

class TestAccount {
    public static void main(String[] args) throws NegativeAmountException {
        System.out.println(args.toString());
        int[] b = new int[5];
        System.out.println(b.toString());
        System.out.println(b.hashCode());
        System.out.println("array b is:"+Arrays.toString(b));
        Account[] accounts = new Account[5];
        accounts[0] = new CurrentAccount(32434, "somename", 3425343);
        accounts[1] = new SavingsAccount(3245, "new name", 23525);
        accounts[2] = new CurrentAccount(2345, "Hello", 23455);
        accounts[3] = new SavingsAccount(36345, "world", 23452);
        accounts[4] = new CurrentAccount(34245, "hello world", 234234);
        System.out.println("Before sorting:"+Arrays.toString(accounts));
        Arrays.sort(accounts);
        System.out.println("After sorting:"+Arrays.toString(accounts));
        Arrays.sort(accounts, new NameComparator());
        System.out.println("After sorting by name:"+Arrays.toString(accounts));
        Arrays.sort(accounts, new Comparator<Account>() {
                                        public int compare(Account ac1, Account ac2) {
                                            return ((Long)ac1.getBalance()).compareTo(ac2.getBalance());
                                        }   
                                    });
        System.out.println("After sorting by anonymous:"+Arrays.toString(accounts));

        Account ac1, ac2;
        ac1 = new CurrentAccount("name1", 10000);
        ac1.display();
        ac2 = new SavingsAccount("another name", 23000);
        ac2.display();
        ac1.deposit(23000);
        ac1.display();
        ac1.withdraw(30000);
        ac1.display();
        ac1.deposit(6000);
        ac1.display();
        ac1.withdraw(3000);
        ac1.display();
        ac2 = null;
        System.out.println("ac1 is "+ac1);
        System.out.println("ac1 is "+ac1.toString());
        System.out.println("ac2 is "+ac2);
//        Rectangle r1 = new Cuboid(7, 5, 4);
//        System.out.println("r1 is "+r1.toString());
        ac1.printPassbook();
        
    }
}



/*
    Some of the methods from the Object class
    public String toString()
    public int hashCode()
        Rectangle r1 = new Rectangle(7, 5);
        Rectangle r2 = new Rectangle(7, 5);
        do not use (r1 == r2) for equality, this is used to check if the two are same
        The Object class has a method as follows:
    public boolean equals(Object o), so we can use
        r1.equals(r2)
    The equals method as defined in the Object class is as follows:
    
        public boolean equals(Object o) {
            return this == o;
        }
*/

