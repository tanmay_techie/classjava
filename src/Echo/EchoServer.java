package Echo;

import java.io.*;
import java.net.*;


public class EchoServer
{
   private ServerSocket board;
   private Socket phone;
   private InputStream receiver;
   private OutputStream transmitter;
   private boolean connected;
   
   public EchoServer(int portno) throws IOException 
   {
      board = new ServerSocket(portno);
   }
   
   public void getAConnection() throws IOException {

       this.phone = this.board.accept();
      this.receiver = this.phone.getInputStream();

     this.transmitter = this.phone.getOutputStream();
  }

   public void serviceConnection() throws IOException {

        int b = this.receiver.read();
       while (b != -1) {

           System.out.print((char)b);
           this.transmitter.write(b);
          b = this.receiver.read();
       }

   }

   public static void main(String[] args) throws IOException {
       int portNo = Integer.parseInt(args[0]);
       EchoServer server = new EchoServer(portNo);
             while (true) {
         server.getAConnection();
         server.serviceConnection();
       }
}
}




