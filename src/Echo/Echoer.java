package Echo;

import java.net.*;
import java.io.*;

public class Echoer implements Runnable {
    private Socket phone;
    private InputStream receiver;
    private OutputStream transmitter;
    private boolean localEcho;

    public Echoer(Socket s) throws IOException {
        this(s.getInputStream(), s.getOutputStream(), true);
        this.phone = s;
    }
    public Echoer(InputStream in, OutputStream out, boolean le) {
        this.receiver = in;
        this.transmitter = out;
        this.localEcho = le;
    }
    public void serviceConnection() throws IOException {
        int b = this.receiver.read();
        while (b != -1) {
            if (localEcho) {
                System.out.print((char)b);
            }
                   this.transmitter.write(b);
            b = this.receiver.read();
        }
        this.receiver.close();
        this.transmitter.close();
        if (this.phone != null) {
            this.phone.close();
        }
    }
    public void run() {
        try {
            this.serviceConnection();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
