package Echo;

import java.net.*;
import java.io.*;

public class TcpClient {
    public static void main(String[] args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        Socket phone = new Socket(host, port);
        Echoer e1 = new Echoer(System.in, phone.getOutputStream(), false);
        Echoer e2 = new Echoer(phone.getInputStream(), System.out, false);
        Thread th1 = new Thread(e1);
        Thread th2 = new Thread(e2);
        th1.start();
        th2.start();
    }
}

