package bank;

import java.util.*;
import java.io.*;

public class Bank implements Serializable {
    private static final long serialVersionUID = -6111002238392072864L;
    private String name;
    private long lastAccountNumber;
    private Map<Long, Account> accountMap = new HashMap<>();

    public Bank(String bn, int bc) {
        this.name = bn;
        this.lastAccountNumber = bc * 10000;
    }
    public long openAccount(Account.Type type, String n, long openBal) throws NegativeAmountException {
        Account ac = type.create(++lastAccountNumber, n, openBal);
        accountMap.put(ac.getAccountNumber(), ac);
        return ac.getAccountNumber();
    }
    private Account getAccount(long acno) throws NoSuchAccountException {
        Account ac = accountMap.get(acno);
        if (ac == null) {
            throw new NoSuchAccountException("invalid ac/no:"+acno);
        }
        return ac;
    }
    public void save() throws IOException {
        FileOutputStream fos = new FileOutputStream(this.name+".bank");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.close();
        fos.close();
    }
    public static Bank load(String bankName) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(bankName+".bank");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Bank bank = (Bank)ois.readObject();
        ois.close();
        fis.close();
        return bank;
    }
    public void display(long acno, PrintWriter out) throws NoSuchAccountException {
        getAccount(acno).display(out);
    }
    public void display(long acno, PrintStream out) throws NoSuchAccountException {
        display(acno, new PrintWriter(out, true));
    }
    public void display(long acno) throws NoSuchAccountException {
        display(acno, System.out);
    }
    public void deposit(long acno, long amt) throws NegativeAmountException, NoSuchAccountException {
        getAccount(acno).deposit(amt);
    }
    public boolean withdraw(long acno, long amt) throws NegativeAmountException, NoSuchAccountException {
        return getAccount(acno).withdraw(amt);
    }
    public void printPassbook(long acno, PrintWriter out) throws NoSuchAccountException {
        getAccount(acno).printPassbook(out);
    }
    public void printPassbook(long acno, PrintStream out) throws NoSuchAccountException {
        printPassbook(acno, new PrintWriter(out, true));
    }
    public void printPassbook(long acno) throws NoSuchAccountException {
        printPassbook(acno, System.out);
    }
    public long closeAccount(long acno) throws NoSuchAccountException {
        Account ac = getAccount(acno);
        accountMap.remove(acno);
        return ac.getBalance();
    }
    public void listAccounts(PrintWriter out) {
        Collection<Account> allAccounts = accountMap.values();
        out.println("List of accounts");
        for (Account account : allAccounts) {
            account.display(out);
        }
        out.println("End of list");
    }
    public void listAccounts(PrintStream out) {
        listAccounts(new PrintWriter(out, true));
    }
    public void listAccounts() {
        listAccounts(System.out);
    }
}
