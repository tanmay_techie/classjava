package bank;

import java.io.File;
import java.util.Date;

public class Dir{

    public static void main(String args[])
    {
      String dirName = args[0];
      File dirFile = new File(dirName);
      if(!dirFile.isDirectory())
      {
        System.out.println(dirName + "is not a directory");
        System.exit(0);
      }
      File[] files = dirFile.listFiles();
      Date fileDate = new Date();
      
      for(File file : files)
      {
        fileDate.setTime(file.lastModified());
        System.out.println(fileDate+"\t"+(file.isDirectory()?"<DIR>":(""+file.length()))+"\t"+file.getName());
      }
       
      
    }
} 

