package bank;

public class CurrentAccount extends Account {
    public CurrentAccount(long acno, String n, long openBal) throws NegativeAmountException {
        super(acno, n, openBal);
    }
    public CurrentAccount(String n, long openBal) throws NegativeAmountException {
        super(n, openBal);
    }
    private static final long minimumBalance = 5000;
    private static final long penaltyAmount = 100;
    public boolean withdraw(long amt) throws NegativeAmountException {
        if (!super.withdraw(amt) ) {
            return false;
        }
        if (this.getBalance() < minimumBalance) {
//            this.balance -= penaltyAmount;
            new Transaction("Penalty", false, penaltyAmount);
        }
        return true;
    }
}
