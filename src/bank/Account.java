package bank;

//import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.*;

/**
  * The Account class represens a bank account. It provides various methods for common
  * operations performed on a bank account.
  */
public abstract class Account extends Object implements Comparable<Account>, Serializable {
    private static final long serialVersionUID = -1969086640876827907L;
    private long accountNumber;
    private String name;
    private long balance;
/*
    private Transaction[] passbook = new Transaction[50];
    private int nextTransactionIndex;
*/
    private List<Transaction> passbook = new ArrayList<>();

    private static long lastAccountNumber = 1001;

/**
  * Constructor with three parameters, could be used to create instances of Account
  * with specific account numbers.
  * @param acno - The account number for the account being created
  * @param n - Name of the account holder
  * @param openBal - Opening balance for the account being created.
  */
    protected Account(long acno, String n, long openBal) throws NegativeAmountException {
/*
        if (openBal < 0) {
            throw new NegativeAmountException("Negative Opening Balance", openBal, this);
        }
*/
        this.accountNumber = acno;
        this.name = n;
//        this.balance = openBal;
        new Transaction("Opening Balance", true, openBal);
    }
    protected Account(String n, long openBal) throws NegativeAmountException {
        this(++lastAccountNumber, n, openBal);
    }

    public enum Type {
        CURRENT {
                public Account create(long acno, String n, long openBal) throws NegativeAmountException {
                    return new CurrentAccount(acno, n, openBal);
                }
            },
        SAVINGS {
                public Account create(long acno, String n, long openBal) throws NegativeAmountException {
                    return new SavingsAccount(acno, n, openBal);
                }
            },
        DEPOSIT {
                public Account create(long acno, String n, long openBal) throws NegativeAmountException {
                    return new Account(acno, n, openBal) {
                        public void deposit(long amt) throws NegativeAmountException {
                            if (amt < 10000) {
                                throw new IllegalArgumentException("amount can't be less than 10000");
                            }
                            super.deposit(amt);
                        }
                    };
                }
            },
        ;
        public abstract Account create(long acno, String n, long openBal) throws NegativeAmountException;
    }
    public class Transaction implements Serializable {
        private long date = System.currentTimeMillis();
        private String naration;
        private boolean credit;
        private long amount;
        Transaction(String n, boolean c, long amt) throws NegativeAmountException {
            if (amt < 0) {
                throw new NegativeAmountException("negative amount", amt, Account.this);
            }
            this.naration = n;
            this.credit = c;
            this.amount = amt;
            Account.this.balance += this.getNetAmount();
/*
            if (passbook.length <= nextTransactionIndex) {
                Account.this.passbook = Arrays.copyOf(Account.this.passbook, Account.this.passbook.length + 20);
            }
            Account.this.passbook[nextTransactionIndex++] = this;
*/
            Account.this.passbook.add(this);
        }
        public long getDate() {
            return this.date;
        }
        public String getNaration() {
            return this.naration;
        }
        public boolean isCredit() {
            return this.credit;
        }
        public long getAmount() {
            return this.amount;
        }
        public long getNetAmount() {
            return this.credit ? amount : -amount;
        }
    } // end of Transaction class

    public synchronized void printPassbook(PrintWriter out) {
        out.println("Passbook of Ac/No "+this.accountNumber);
        long runningBalance = 0;
        for (Transaction t : passbook) {
/*
            if (t == null) {
                break;
            }
*/
            runningBalance += t.getNetAmount();
            out.printf("%1$td-%1$tm-%1$tY  %2$15s  %3$12d  %4$12d  %5$12d\r\n",
                        t.getDate(), 
                        t.getNaration(),
                        t.credit ? t.getAmount() : 0,
                        t.credit ? 0 : t.getAmount(),
                        runningBalance
                        );
        }
        out.println("End of passbook");
    }
    public void printPassbook(PrintStream out) {
        printPassbook(new PrintWriter(out, true));
    }
    public void printPassbook() {
        printPassbook(System.out);
    }
    public int compareTo(Account ac) {
        return ((Long)this.accountNumber).compareTo(ac.accountNumber);
    }
    public String toString() {
        return "Account:"+this.accountNumber+","+this.name+","+this.getBalance();
    }
    public long getAccountNumber() {
        return this.accountNumber;
    }
    public String getName() {
        return this.name;
    }
    public synchronized long getBalance() {
        return this.balance;
    }
    public synchronized void deposit(long amt) throws NegativeAmountException {
/*
        if (amt < 0) {
            throw new NegativeAmountException("Negative Deposit", amt, this);
        }
        this.balance += amt;
*/
        new Transaction("Deposit", true, amt);
        this.notifyAll();
    }
/**
  * The withdraw method is used for withdrawing amount from the balance for the account.
  * @param amt - The amount to be withdrawn.
  * @return It returns false in case the account does not have sufficient balance, and true
  * otherwise.
  * @see CurrentAccount
  */
    public synchronized boolean withdraw(long amt) throws NegativeAmountException {
/*
        if (amt < 0) {
            throw new NegativeAmountException("Negative Withdrawal", amt, this);
        }
*/
        while (amt > this.balance) {
            try {
                this.wait();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
//        this.balance -= amt;
        new Transaction("Withdrawal", false, amt);
        return true;
    }
    public void display(PrintWriter out) {
        out.println("Account:"+this.accountNumber+","+this.name+","+this.getBalance());
    }
    public void display(PrintStream out) {
        display(new PrintWriter(out,true));
    }
    public void display() {
        display(System.out);
    }
}

