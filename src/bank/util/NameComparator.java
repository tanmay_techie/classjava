package bank.util;

import bank.Account;
import java.util.Comparator;

public class NameComparator implements Comparator<Account> {
    public int compare(Account ac1, Account ac2) {
        return ac1.getName().compareTo(ac2.getName());
    }
}
