package bank;

import java.net.*;
import java.io.*;

public class BankServer {
    private ServerSocket board;
    private Bank bank;

    public BankServer(int port, Bank bank) throws IOException {
        this.bank = bank;
        this.board = new ServerSocket(port);
    }
    public void run() throws IOException {
        while(true) {
            Socket phone = board.accept();
            Runnable clerk = new BankClerk(this.bank, phone.getInputStream(), phone.getOutputStream());
            Thread thread = new Thread(clerk);
            thread.start();
        }
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        int portNo = Integer.parseInt(args[0]);
        String bankName = args[1];
        File bankFile = new File(mbankName+".bank");
        Bank bank = null;
        if (bankFile.exists()) {
            bank = Bank.l                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             oad(bankName);
        } else {
            bank = new Bank(bankName, Integer.parseInt(args[2]));
        }
        BankServer server = new BankServer(portNo, bank);
        server.run();
    }
}
