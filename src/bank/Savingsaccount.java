package bank;

public class SavingsAccount extends Account {
    public SavingsAccount(int acno, String n, long openBal) throws NegativeAmountException {
        super(acno, n, openBal);
    }
    public SavingsAccount(String n, long openBal) throws NegativeAmountException {
        super(n, openBal);
    }
}
