package bank;

public class NegativeAmountException extends Exception {
    private long amount;
    private Account account;

    NegativeAmountException(String msg, long amt, Account ac) {
        super(msg);
        this.amount = amt;
        this.account = ac;
    }
    public long getAmount() {
        return this.amount;
    }
    public Account getAccount() {
        return this.account;
    }
    public String toString() {
        return super.toString()+":"+this.amount+":"+this.account;
    }
}

