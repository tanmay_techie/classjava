package bank;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.net.*;

public class BankClerk implements Runnable {
    private Bank bank;
    private Scanner commandScanner;
    private PrintWriter responseWriter;

    public BankClerk(Bank b, InputStream in, OutputStream out) {
        this.bank = b;
        this.commandScanner = new Scanner(in);
        this.responseWriter = new PrintWriter(out, true);
    }
    public BankClerk(Bank b) {
        this(b, System.in, System.out);
    }

    public enum Command {
        OPEN("[Oo][Pp][Ee][Nn]\\s+([SsCcDd])\\s+(\\p{javaLetter}+)\\s(\\d+)"),
        DEPOSIT("[Dd][Ee][Pp][Oo][Ss][Ii][Tt]\\s+(\\d+)\\s+(\\d+)"),
        WITHDRAW("[Ww][Ii][Tt][Hh][Dd][Rr][Aa][Ww]\\s+(\\d+)\\s+(\\d+)"),
        DISPLAY("[Dd][Ii][Ss][Pp][Ll][Aa][Yy]\\s+(\\d+)"),
        PASSBOOK("[Pp][Aa][Ss][Ss][Bb][Oo][Oo][Kk]\\s+(\\d+)"),
        CLOSE("[Cc][Ll][Oo][Ss][Ee]\\s+(\\d+)"),
        LIST("[Ll][Ii][Ss][Tt]"),
        SAVE("[Ss][Aa][Vv][Ee]"),
        QUIT("[Qq][Uu][Ii][Tt]"),
        ;
        private Pattern syntax;
        Command(String s) {
            this.syntax = Pattern.compile(s);
        }
        public Pattern getSyntax() {
            return this.syntax;
        }
        public static Command getCommand(Matcher m) {
            Command[] allCommands = Command.values();
            for (Command command : allCommands) {
                if (m.usePattern(command.getSyntax()).matches()) {
                    return command;
                }
            }
            return null;
        }
    }

    public void run() {
        Matcher matcher = Pattern.compile("").matcher("");
        Command command = null;
        while((command = (Command.getCommand(matcher.reset(commandScanner.nextLine())))) != Command.QUIT) {
            if (command == null) {
                responseWriter.println("invalid command");
                continue;
            }
            switch(command) {
                case OPEN:
                    char typeChar = matcher.group(1).charAt(0);
                    String name = matcher.group(2);
                    long amt = Long.parseLong(matcher.group(3));
                    Account.Type type = null;
                    switch(typeChar) {
                        case 'C':
                        case 'c':
                            type=Account.Type.CURRENT;
                            break;
                        case 'S':
                        case 's':
                            type=Account.Type.SAVINGS;
                            break;
                        case 'D':
                        case 'd':
                            type=Account.Type.DEPOSIT;
                            break;
                    }
                    long acno = 0;
                    try {
                        acno = bank.openAccount(type, name, amt);
                        bank.display(acno, responseWriter);
                    } catch (NegativeAmountException | NoSuchAccountException ne) {
                        responseWriter.println(ne.getMessage());
                    }
                    continue;
                case DEPOSIT:
                    acno = Long.parseLong(matcher.group(1));
                    amt = Long.parseLong(matcher.group(2));
                    try {
                        bank.deposit(acno, amt);
                        bank.display(acno, responseWriter);
                    } catch (NegativeAmountException | NoSuchAccountException ne) {
                        responseWriter.println(ne.getMessage());
                    }
                    continue;
                case WITHDRAW:
                    acno = Long.parseLong(matcher.group(1));
                    amt = Long.parseLong(matcher.group(2));
                    try {
                        if (!bank.withdraw(acno, amt)) {
                            responseWriter.println("insufficient balance");
                        }
                        bank.display(acno, responseWriter);
                    } catch (NegativeAmountException | NoSuchAccountException ne) {
                        responseWriter.println(ne.getMessage());
                    }
                    continue;
                case DISPLAY:
                    acno = Long.parseLong(matcher.group(1));
                    try {
                        bank.display(acno, responseWriter);
                    } catch (NoSuchAccountException ne) {
                        responseWriter.println(ne.getMessage());
                    }
                    continue;
                case PASSBOOK:
                    acno = Long.parseLong(matcher.group(1));
                    try {
                        bank.printPassbook(acno, responseWriter);
                    } catch (NoSuchAccountException ne) {
                        responseWriter.println(ne.getMessage());
                    }
                    continue;
                case CLOSE:
                    acno = Long.parseLong(matcher.group(1));
                    try {
                        amt = bank.closeAccount(acno);
                        responseWriter.println("closing balance:"+amt);
                    } catch (NoSuchAccountException ne) {
                        responseWriter.println(ne.getMessage());
                    }
                    continue;
                case LIST:
                    bank.listAccounts(responseWriter);
                    continue;
                case SAVE:
                    try {
                    bank.save();
                    } catch (IOException ioe) {
                        responseWriter.println(ioe.getMessage());
                        continue;
                    }
                    responseWriter.println("Saved successfully");
                    continue;
            }
        }
    }
    public static void main(String[] args) throws Exception {
        int portNo = Integer.parseInt(args[0]);
        String bankName = args[1];
        File bankFile = new File(bankName+".bank");
        Bank bank = null;
        if (bankFile.exists()) {
            bank = Bank.load(bankName);
        } else {
            bank = new Bank(bankName, Integer.parseInt(args[2]));
        }
        ServerSocket board = new ServerSocket(portNo);
        while (true) {
            Socket phone = board.accept();
            BankClerk clerk = new BankClerk(bank, phone.getInputStream(), phone.getOutputStream());
            clerk.run();
        }
    }
}
