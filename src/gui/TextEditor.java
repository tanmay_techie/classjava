package gui;

import java.awt.*;

public class TextEditor extends Frame 
{
     private Button newButton = new Button("New");
     private Button openButton = new Button("Open");
     private Button saveButton = new Button("Save");
     private Button saveasButton = new Button("Save As");
     private Button exitButton = new Button("Exit");
     private Panel statusbar = new Panel();
     private TextField statusfield = new TextField();
     private TextField helpfield = new TextField();
     private TextField clockfield = new TextField();
     private TextArea editorArea = new TextArea();

public TextEditor()
{
   super("untitled");
   setBounds(0,0,500,600);
   setupComponents();
   setupMenus();
   setupClock();  
}

private void setupComponents()
{
    Panel toolbar = new Panel();
    this.add(toolbar,BorderLayout.NORTH);
    this.add(editorArea,BorderLayout.CENTER);
    this.add(statusbar,BorderLayout.SOUTH);
    toolbar.setLayout(new FlowLayout(FlowLayout.LEFT));
    toolbar.add(newButton);
    toolbar.add(openButton);
    toolbar.add(saveButton);
    toolbar.add(saveasButton);
    toolbar.add(exitButton);
    statusbar.add(statusfield);
    statusbar.add(helpfield);
    statusbar.add(clockfield);
    statusbar.setLayout(new GridLayout());
    toolbar.setBackground(Color.RED);
    newButton.setBackground(Color.CYAN);
    openButton.setBackground(Color.CYAN);
    saveButton.setBackground(Color.CYAN);
    saveasButton.setBackground(Color.CYAN);
    exitButton.setBackground(Color.CYAN);
}
private void setupMenus()
{
}
private void setupClock() {
        Thread th = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        clockfield.setText(String.format("%tT", System.currentTimeMillis()));
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                }
            });
        th.setDaemon(true);
        th.start();
    }
public static void main(String[] args)
{
  TextEditor editor = new TextEditor();
  editor.setVisible(true); 
}
}
