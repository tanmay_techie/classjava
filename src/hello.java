/**
* The HelloWorld class is used as the first class, for learning how to write java applications.
* This class has only the main method, which would simply print Hello World on the console.
*/  


class HelloWorld{
/**
* This method prints Hello World on the standard output. it has parameter of type String[]
* which is used to pass the command line arguments.
*/
   public static void main(String[] a){

        System.out.println("Hello World");    
    }
}
class AnotherHelloWOrld{    
        public static void main(String[] b){

            System.out.println("Another Hello World");

            for(int i=0;i<b.length;i++){
            System.out.println("Hello  "+b[i]);
            }
   }
 }
